from string import Template
import argparse
import os
import platform
import sys
import subprocess

# This class lets you specify the os version, compilers, and mapping from architectures to sdks,
# for which you want to build.


class AppleConfig:
    compilers = []
    arch_sdk_map = []
    arch_build = ''
    os_version = ''
    os_name = ''  # Differs from platform name due to case-sensitivity

    def __init__(self, compilers, arch_sdk_map, os_version, os_name):
        self.compilers = compilers
        self.arch_sdk_map = arch_sdk_map
        self.os_version = os_version
        self.os_name = os_name

    def update_arch(self):
        self.arch_build = self.get_default_arch()

    def get_default_arch(self):
        system = platform.system().lower()
        machine = platform.machine().lower()

        if system == 'darwin':
            if 'arm' in machine:
                return 'armv8'
            elif 'x86_64' in machine:
                return 'x86_64'

        raise ValueError(
            "Unable to determine the architecture for the current system.")


all_configs = {
    'macos': AppleConfig(
        ['apple-clang-15.0'],
        {
            'armv8': 'macosx',
            'x86_64': 'macosx'
        },
        '11.0',
        'Macos'
    ),
    'ios': AppleConfig(
        ['apple-clang-15.0'],
        {
            'x86_64': 'iphonesimulator',
            'armv8': 'iphoneos'
        },
        '13.0',
        'iOS'
    ),
    'tvos': AppleConfig(
        ['apple-clang-15.0'],
        {
            'x86_64': 'appletvsimulator',
            'armv8': 'appletvos'
        },
        '13.0',
        'tvOS'
    ),
}

all_build_types = [
    'Release',
    'RelWithDebInfo',
    'Debug'
]

all_address_sanitizers = [
    True,
    False
]


def cmd_output(cmd):
    try:
        return subprocess.check_output(cmd.split(' ')).decode().strip()
    except subprocess.CalledProcessError:
        return None


def get_default():
    return 'macos-11.0-armv8-apple-clang-15.0-Release'


def find_sdkroot(platform, arch):
    sdk = all_configs[platform].arch_sdk_map[arch]
    sdkroot_cmd = f'xcrun --sdk {sdk} --show-sdk-path'

    sdkroot = cmd_output(sdkroot_cmd)
    return sdkroot


def create_profile(args):
    if args.build_type not in all_build_types:
        raise RuntimeError("Unknown build type: {}".format(args.build_type))

    if args.platform not in all_configs:
        raise RuntimeError("Unknown platform: {}".format(args.platform))

    platform_config = all_configs[args.platform]
    platform_config.update_arch()

    if args.compiler not in platform_config.compilers:
        raise RuntimeError("Unknown compiler {}, for platform {}".format(
            args.compiler, args.platform))

    if args.arch not in platform_config.arch_sdk_map:
        raise RuntimeError("Unknown arch {}, for platform {}".format(
            args.arch, args.platform))

    compiler_name, compiler_version = tuple(args.compiler.split('-')[-2:])

    template_path = os.path.join(os.path.dirname(__file__), 'apple.template')
    with open(template_path, 'rb') as template_file:
        profile_template = template_file.read().decode('utf-8')

    ar = ''
    cc = ''
    if args.platform.lower() == 'macos':
        # on Mac, just find the compiler name.
        cc = cmd_output(f'which {compiler_name}')
        ar = cmd_output('which ar')
    else:
        # On other platforms, find the non-simulator compiler, i.e armv8 (not x86_64)
        compiler_sdk = platform_config.arch_sdk_map['armv8']
        cc = cmd_output(f'xcrun --find --sdk {compiler_sdk} {compiler_name}')
        ar = cmd_output(f'xcrun --find --sdk {compiler_sdk} ar')

    if not cc:
        print(f'Can\'t find C compiler for {compiler_name}, skipping')
        return None

    cxx_compilers = []
    if 'gcc' in args.compiler:
        cxx_compilers = [
            compiler_name.replace('gcc', 'g++'),
        ]
    elif 'clang' in args.compiler:
        cxx_compilers = [
            compiler_name.replace('clang', 'clang++')
        ]

    cxx = next(filter(lambda cxx: cxx, map(lambda cxx_compiler: cmd_output(f'which {cxx_compiler}'),
                                           cxx_compilers)), None)
    if not cxx:
        print(
            f'Can\'t find C++ compiler for {args.compiler}, tried: {cxx_compilers}, skipping')
        return None

    sdkroot = find_sdkroot(args.platform, args.arch)
    if not sdkroot:
        print(
            f'Can\'t find SDK root path for {args.platform}-{args.arch}, skipping')
        return None

    conan_env = {
        'AR': ar,
        'CC': cc,
        'CXX': cxx,
        'SDKROOT': sdkroot,
    }

    extra_compiler_settings = []

    if args.address_sanitizer:
        extra_compiler_settings.append('compiler.address_sanitizer=True')

        conan_env.update({
            'CFLAGS': '-fsanitize=address',
            'CXXFLAGS': '-fsanitize=address',
            'LDFLAGS': '-fsanitize=address'
        })

    profile = Template(profile_template).substitute({
        'os': platform_config.os_name,
        'os_version': platform_config.os_version,
        'sdk': platform_config.arch_sdk_map[args.arch],
        'arch': args.arch,
        'arch_build': platform_config.arch_build,
        'build_type': args.build_type,
        'compiler_version': compiler_version,
        'extra_compiler_settings': '\n'.join(extra_compiler_settings),
        'env': '\n'.join(map(lambda e: f'{e[0]}={e[1]}', conan_env.items()))
    })

    return profile


def main():
    argParser = argparse.ArgumentParser(
        description="Create conan profile for building libraries for specific Apple target")
    argParser.add_argument("--build-type", default="Release", help="Specify build type")
    argParser.add_argument("--platform", default="macos", help="Specify target platform")
    argParser.add_argument("--compiler", default="apple-clang-15.0", help="Specify compiler to use")
    argParser.add_argument("--arch", default="x86_64", help="Specify arch to target")

    args = argParser.parse_args()

    profile = create_profile(args)
    print(profile)


if __name__ == "__main__":
    try:
        sys.exit(main())
    except RuntimeError as err:
        print("Error: {}".format(str(err)))
