#!/bin/python

import os
import sys

# ---------------------------------------------------------------------------------------
# This is a hack to allow create_all_apple_conan_profiles.py to be callable directly
# so that the legacy systems not using the top-level entry-point keep working.
# We should delete this as soon as we've transitioned everything onto the new entry-point
if __name__ == "__main__":
    dir = os.path.dirname(os.path.dirname(__file__))
    sys.path.append(dir)
# ---------------------------------------------------------------------------------------

from apple.create_apple_conan_profile import all_configs as all_apple_configs
from apple.create_apple_conan_profile import all_build_types as all_apple_build_types
from apple.create_apple_conan_profile import create_profile as create_apple_profile
from apple.create_apple_conan_profile import get_default as get_default_apple_profile
from apple.create_apple_conan_profile import all_address_sanitizers as all_apple_address_sanitizers

from collections import namedtuple

import pathlib
import argparse


def get_default_conan_user_home():
    if 'CONAN_USER_HOME' in os.environ:
        return os.path.join(os.environ['CONAN_USER_HOME'], '.conan')

    return os.path.join(pathlib.Path.home(), '.conan')


def create_profiles(main_args):
    conan_user_home = get_default_conan_user_home()
    conan_profiles_dir = os.path.join(conan_user_home, 'profiles')

    if not os.path.exists(conan_user_home):
        os.mkdir(conan_user_home)

    if not os.path.exists(conan_profiles_dir):
        os.mkdir(conan_profiles_dir)

    configs_to_use = all_apple_configs
    if (hasattr(main_args, 'platform') and main_args.platform != 'apple'):
        configs_to_use = {main_args.platform: all_apple_configs[main_args.platform]}

    default_profile = get_default_apple_profile() if not main_args.no_default else None

    for build_type in all_apple_build_types:
        for platform in configs_to_use:
            config = all_apple_configs[platform]
            for compiler in config.compilers:
                for arch in config.arch_sdk_map:
                    for address_sanitizer in all_apple_address_sanitizers:
                        # @fixme change profile names if and when we start to build stuff for two
                        # different sdk versions of the same arch.
                        os_version = config.os_version
                        profile_name = f'{platform.lower()}-{os_version}-{arch}-{compiler}-{build_type}'

                        if address_sanitizer:
                            profile_name += "-asan"

                        profile_path = os.path.join(conan_profiles_dir, profile_name)

                        print(f"Creating conan profile {profile_name}")

                        if os.path.exists(profile_path):
                            print(f'Profile {profile_name} already exists, overwriting')

                        args = namedtuple(
                            "create_args", ["build_type", "platform", "compiler", "arch", "address_sanitizer"])
                        args.build_type = build_type
                        args.platform = platform
                        args.compiler = compiler
                        args.arch = arch
                        args.address_sanitizer = address_sanitizer
                        profile = create_apple_profile(args)
                        if not profile:
                            print(f'Unable to create profile {profile_name}, skipping')
                            continue

                        with open(profile_path, 'wt') as profile_file:
                            profile_file.write(profile)

                        if default_profile and profile_name == default_profile:
                            print(f'Using {profile_name} as default profile')
                            with open(os.path.join(conan_profiles_dir, 'default'), 'wt') as profile_file:
                                profile_file.write(profile)


def main():
    argParser = argparse.ArgumentParser(
        description="Create all apple conan profiles")
    argParser.add_argument("--no-default", default=False,
                           action="store_true", help="Try to guess default profile")

    main_args = argParser.parse_args()
    create_profiles(main_args)


if __name__ == "__main__":
    try:
        sys.exit(main())
    except RuntimeError as err:
        print("Error: {}".format(str(err)))
