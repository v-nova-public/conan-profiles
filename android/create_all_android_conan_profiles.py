#!/bin/python

import os
import sys

# ---------------------------------------------------------------------------------------
# This is a hack to allow create_all_android_conan_profiles.py to be callable directly
# so that the legacy systems not using the top-level entry-point keep working.
# We should delete this as soon as we've transitioned everything onto the new entry-point
if __name__ == "__main__":
    dir = os.path.dirname(os.path.dirname(__file__))
    sys.path.append(dir)
# ---------------------------------------------------------------------------------------

from android.create_android_conan_profile import create_profile as create_android_profile
from android.create_android_conan_profile import all_abis as all_android_abis
from android.create_android_conan_profile import all_build_types as all_android_build_types
from android.create_android_conan_profile import all_api_levels as all_android_api_levels
from android.create_android_conan_profile import all_address_sanitizers as all_android_address_sanitizers

from collections import namedtuple

import pathlib


def get_default_conan_user_home():
    if 'CONAN_USER_HOME' in os.environ:
        return os.path.join(os.environ['CONAN_USER_HOME'], '.conan')

    return os.path.join(pathlib.Path.home(), '.conan')


def main():
    conan_user_home = get_default_conan_user_home()
    conan_profiles_dir = os.path.join(conan_user_home, 'profiles')

    if not os.path.exists(conan_user_home):
        os.mkdir(conan_user_home)

    if not os.path.exists(conan_profiles_dir):
        os.mkdir(conan_profiles_dir)

    for build_type in all_android_build_types:
        for abi in all_android_abis:
            for api_level in all_android_api_levels:
                for address_sanitizer in all_android_address_sanitizers:
                    profile_name = f'android-{abi}-api-{api_level}-{build_type}'

                    if address_sanitizer:
                        profile_name += "-asan"

                    profile_path = os.path.join(conan_profiles_dir, profile_name)

                    print(f"Creating conan profile {profile_name}")
                    if os.path.exists(profile_path):
                        print(f'Profile {profile_name} already exists, overwriting')

                    args = namedtuple(
                        "create_args",
                        ["abi", "api_level", "build_type", "ndk_path", "address_sanitizer"]
                    )
                    args.abi = abi
                    args.api_level = api_level
                    args.build_type = build_type
                    args.ndk_path = 'USE_ENV_VAR'
                    args.address_sanitizer = address_sanitizer
                    profile = create_android_profile(args)
                    with open(profile_path, 'wt') as profile_file:
                        profile_file.write(profile)


if __name__ == "__main__":
    try:
        sys.exit(main())
    except RuntimeError as err:
        print("Error: {}".format(str(err)))
