from string import Template
import argparse
import os
import sys
import subprocess
import re

from conans.client.tools import detected_os, detected_architecture

all_build_types = [
    'Release',
    'RelWithDebInfo',
    'Debug'
]

# TODO figure out what needs to be done to support armv6, armv7hf archs
all_abis = [
    'armeabi-v7a',
    'arm64-v8a',
    'x86',
    'x86_64'
]

all_api_levels = [
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30
]

all_address_sanitizers = [
    True,
    False
]


def cmd_output(cmd):
    try:
        return subprocess.check_output(cmd.split(' ')).decode().strip()
    except subprocess.CalledProcessError:
        return None


def get_compiler_version(compiler_path):
    compiler_version_output = cmd_output(
        f'{compiler_path} --version').split(' ')
    for idx, word in enumerate(compiler_version_output):
        if word == 'version':
            if idx < len(compiler_version_output) - 1:
                return compiler_version_output[idx + 1]
            break
    return None


def create_profile(args):
    template_path = os.path.join(os.path.dirname(__file__), 'android.template')
    with open(template_path, 'rb') as template_file:
        profile_template = template_file.read().decode('utf-8')

    ndk_path = os.environ['ANDROID_NDK_PATH'] if args.ndk_path == 'USE_ENV_VAR' else args.ndk_path

    if args.abi not in all_abis:
        raise RuntimeError(f'Unsupported android abi: {args.abi}')

    if args.build_type not in all_build_types:
        raise RuntimeError(
            f'Unsupported android build type: {args.build_type}')

    if int(args.api_level) not in all_api_levels:
        raise RuntimeError(f'Unsupported android api level: {args.api_level}')

    with open(os.path.join(ndk_path, 'source.properties')) as f:
        m = re.match(r"Pkg.Desc = Android NDK\s+Pkg.Revision = (\d+)\.(\d+).(\d+)", f.read())
        if not m:
            raise RuntimeError('Unrecognized NDK source.properties:')
        ndk_version = tuple(map(int, m.groups()))

    compiler = 'clang'
    if detected_os() == 'Windows':
        os_directory = 'windows-x86_64'
    elif detected_os() == 'Macos':
        os_directory = 'darwin-x86_64'
    else:
        os_directory = 'linux-x86_64'
    compiler_path = os.path.join(
        ndk_path, 'toolchains', 'llvm', 'prebuilt', os_directory, 'bin', compiler)
    compiler_version = get_compiler_version(compiler_path)
    compiler_major_version = int(compiler_version.split('.')[0])

    conan_os_build = detected_os()
    conan_arch_build = detected_architecture()

    neon_support = False
    if args.abi == 'armeabi-v7a':
        abi_short = 'armeabi-v7a'
        conan_arch = 'armv7'
        android_arch = 'arm'
        neon_support = True
        target_host = 'arm-linux-androideabi'
        ndk_toolchain_name = 'arm-linux-androideabi-clang'
        compiler_prefix = 'armv7a-linux-androideabi'
    elif args.abi == 'arm64-v8a':
        abi_short = 'arm64-v8a'
        conan_arch = 'armv8'
        android_arch = 'arm64'
        neon_support = True
        target_host = 'aarch64-linux-android'
        ndk_toolchain_name = 'aarch64-linux-android-clang'
        compiler_prefix = target_host
    elif args.abi == 'x86':
        abi_short = 'x86'
        conan_arch = 'x86'
        android_arch = 'x86'
        target_host = 'i686-linux-android'
        ndk_toolchain_name = 'x86-clang'
        compiler_prefix = target_host
    elif args.abi == 'x86_64':
        abi_short = 'x86_64'
        conan_arch = 'x86_64'
        android_arch = 'x86_64'
        target_host = 'x86_64-linux-android'
        ndk_toolchain_name = 'x86_64-clang'
        compiler_prefix = target_host
    else:
        raise RuntimeError("Unsupported abi={} requested".format(args.abi))

    tool_prefix = 'llvm' if ndk_version[0] > 22 else target_host
    linker = 'lld' if ndk_version[0] > 22 else (target_host + '-ld')

    os_arch_subdir = f'{conan_os_build.lower()}-{conan_arch_build}'
    ndk_toolchain_path = os.path.join(
        ndk_path, 'toolchains', 'llvm', 'prebuilt', os_arch_subdir)
    ndk_llvm_toolchain = os.path.join(
        ndk_path, 'toolchains', 'llvm', 'prebuilt', os_arch_subdir)

    ndk_sysroot = os.path.join(ndk_llvm_toolchain, 'sysroot')

    extra_compiler_settings = []
    extra_cflags = ''
    extra_cxxflags = ''
    extra_ldflags = ''

    if args.address_sanitizer:
        extra_compiler_settings.append('compiler.address_sanitizer=True')
        extra_cflags = '-fsanitize=address'
        extra_cxxflags = '-fsanitize=address'
        extra_ldflags = '-fsanitize=address'

    profile = Template(profile_template).substitute({
        'ndk_path': ndk_path,
        'conan_arch': conan_arch,
        'conan_arch_build': conan_arch_build,
        'conan_os_build': conan_os_build,
        'android_arch': android_arch,
        'target_host': target_host,
        'compiler_prefix': compiler_prefix,
        'compiler': compiler,
        'tool_prefix': tool_prefix,
        'linker': linker,
        'compiler_version': compiler_major_version,
        'android_abi': abi_short,
        'android_api_level': args.api_level,
        'build_type': args.build_type,
        'neon_cmake_args': '-DANDROID_ARM_NEON=ON' if neon_support else '',
        'ndk_toolchain_name': ndk_toolchain_name,
        'ndk_toolchain_path': ndk_toolchain_path,
        'ndk_llvm_toolchain': ndk_llvm_toolchain,
        'ndk_sysroot': ndk_sysroot,
        'extra_compiler_settings': '\n'.join(extra_compiler_settings),
        'extra_cflags': extra_cflags,
        'extra_cxxflags': extra_cxxflags,
        'extra_ldflags': extra_ldflags,
    })

    return profile


def main():
    argParser = argparse.ArgumentParser(
        description="Create conan profile for building libraries for specific android arch/api/abi")
    argParser.add_argument("--ndk-path", default="USE_ENV_VAR",
                           help="Specify the path to your NDK installation, when USE_ENV_VAR is specified then the environment variable ANDROID_NDK_PATH will be used")
    argParser.add_argument("--abi", default="armeabi-v7a",
                           help="Specify the android ABI to target, choices are ['armeabi-v7a', 'arm64-v8a', 'x86', 'x86_64']")
    argParser.add_argument("--api-level", default="26",
                           help="Specify the android API level, recommend reasonably new")
    argParser.add_argument("--build-type", default="Release", help="Specify build type")

    args = argParser.parse_args()
    profile = create_profile(args)
    print(profile)


if __name__ == "__main__":
    try:
        sys.exit(main())
    except RuntimeError as err:
        print("Error: {}".format(str(err)))
