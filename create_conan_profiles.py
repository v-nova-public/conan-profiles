#!/bin/python

import sys
import argparse

from android.create_all_android_conan_profiles import main as create_all_android_profiles
from apple.create_all_apple_conan_profiles import create_profiles as create_all_apple_profiles
from linux.create_all_linux_conan_profiles import create_profiles as create_all_linux_profiles
from windows.create_all_windows_conan_profiles import create_profiles as create_all_windows_profiles

from collections import namedtuple


def is_apple(platform):
    return platform.lower() in ["apple", "ios", "macos", "tvos"]


def main():
    argParser = argparse.ArgumentParser(
        description="Create conan profiles for compiling libraries on the required platform")
    argParser.add_argument("--platform",			required=True,
                           help="Specify platform to generate conan profiles for")
    argParser.add_argument("--no-default",			default=False,  action="store_true",
                           help="Don't create a default conan profile on this platform")

    args = argParser.parse_args()

    print(f"Attempting to create conan profiles for platform: {args.platform}")
    if args.platform == "android":
        if args.no_default == True:
            print("The '--no-default' option is not currently supported on android")
        create_all_android_profiles()
    elif is_apple(args.platform):
        create_all_apple_profiles(args)
    elif args.platform == "linux":
        create_all_linux_profiles(args)
        # Windows profiles are needed to package binaries in some linux jobs
        # TODO Make this more specific or properly fix the missing profiles
        create_all_windows_profiles(args)
    elif args.platform == "windows":
        create_all_windows_profiles(args)
    else:
        raise Exception("""
            Could not create conan profiles: unsupported platform '{}'. Note that platforms are
            lower-case (for example, 'ios' not 'iOS').
            """.format(args.platform))


if __name__ == "__main__":
    try:
        sys.exit(main())
    except RuntimeError as err:
        print("Error: {}".format(str(err)))
