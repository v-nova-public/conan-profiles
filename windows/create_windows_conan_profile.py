from string import Template
import argparse
import os
import sys

import subprocess

all_compilers = [
    'vs-2017',
    'vs-2019',
    'vs-2022',
    'msvc-192',
    'msvc-193',
]

all_runtimes = [
    'MT',
    'MD'
]

all_build_types = [
    'Release',
    'RelWithDebInfo',
    'Debug'
]

all_address_sanitizers = [
    True,
    False
]


def cmd_output(cmd):
    return subprocess.check_output(cmd.split(' ')).decode().strip()


def create_profile(args):
    if args.compiler not in all_compilers:
        raise RuntimeError("Unknown compiler: {}".format(args.compiler))

    if args.runtime not in all_runtimes:
        raise RuntimeError("Unknown compiler runtime: {}".format(args.runtime))

    if args.build_type not in all_build_types:
        raise RuntimeError("Unknown build type: {}".format(args.build_type))

    compiler_name, compiler_ver = tuple(args.compiler.split('-')[-2:])
    if compiler_name == 'vs':
        if compiler_ver == '2017':
            compiler_version = 15
        elif compiler_ver == '2019':
            compiler_version = 16
        elif compiler_ver == '2022':
            compiler_version = 17
        else:
            raise RuntimeError(
                f'Unsupported VS compiler version: {compiler_ver}')
    elif compiler_name == 'msvc':
        compiler_version = compiler_ver
        if compiler_version not in ['192', '193']:
            raise RuntimeError(
                f'Unsupported msvc compiler version: {compiler_ver}')

    compiler_full_name = 'Visual Studio' if compiler_name == 'vs' else compiler_name
    compiler_runtime = args.runtime if args.build_type != 'Debug' else f'{args.runtime}d'

    template_path = os.path.join(os.path.dirname(__file__), 'windows.template')
    with open(template_path, 'rb') as template_file:
        profile_template = template_file.read().decode('utf-8')

    conan_os = 'Windows'
    conan_arch = 'x86_64'
    conan_arch_build = conan_arch

    conan_env = {
        'CC': 'cl',
        'CXX': 'cl',
    }

    extra_compiler_settings = []

    if compiler_name == 'vs':
        extra_compiler_settings.append(f'compiler.runtime={compiler_runtime}')
    else:
        runtime = 'static' if compiler_runtime == 'MT' else 'dynamic'
        runtime_type = args.build_type
        extra_compiler_settings.append('compiler.update=None')
        extra_compiler_settings.append(f'compiler.runtime={runtime}')
        extra_compiler_settings.append(f'compiler.runtime_type={runtime_type}')
        extra_compiler_settings.append('compiler.cppstd=14')
        extra_compiler_settings.append('compiler.toolset=None')

    if args.address_sanitizer:
        extra_compiler_settings.append('compiler.address_sanitizer=True')

        conan_env.update({
            'CFLAGS': '-fsanitize=address -Zi -FS',
            'CXXFLAGS': '-fsanitize=address -Zi -FS',
            'LDFLAGS': '-fsanitize=address'
        })

    profile = Template(profile_template).substitute({
        'os': conan_os,
        'arch': conan_arch,
        'arch_build': conan_arch_build,
        'build_type': args.build_type,
        'compiler_name': compiler_full_name,
        'compiler_version': compiler_version,
        'extra_compiler_settings': '\n'.join(extra_compiler_settings),
        'env': '\n'.join(map(lambda e: f'{e[0]}={e[1]}', conan_env.items()))
    })
    return profile


def main():
    argParser = argparse.ArgumentParser(
        description="Create conan profile for building libraries for specific android arch/api/abi")
    argParser.add_argument("--build-type", default="Release", help="Specify build type")
    argParser.add_argument("--compiler", required=True, help="Specify compiler to use")
    argParser.add_argument("--runtime", required=True, help="Specify compiler runtime to use")

    args = argParser.parse_args()
    profile = create_profile(args)
    print(profile)


if __name__ == "__main__":
    try:
        sys.exit(main())
    except RuntimeError as err:
        print("Error: {}".format(str(err)))
