#!/bin/python

import os
import sys

# ---------------------------------------------------------------------------------------
# This is a hack to allow create_all_windows_conan_profiles.py to be callable directly
# so that the legacy systems not using the top-level entry-point keep working.
# We should delete this as soon as we've transitioned everything onto the new entry-point
if __name__ == "__main__":
    dir = os.path.dirname(os.path.dirname(__file__))
    sys.path.append(dir)
# ---------------------------------------------------------------------------------------

from windows.create_windows_conan_profile import create_profile as create_windows_profile
from windows.create_windows_conan_profile import all_compilers as all_windows_compilers
from windows.create_windows_conan_profile import all_runtimes as all_windows_runtimes
from windows.create_windows_conan_profile import all_build_types as all_windows_build_types
from windows.create_windows_conan_profile import all_address_sanitizers as all_windows_address_sanitizers

from collections import namedtuple

from conans.client.tools import detected_os

import pathlib
import argparse


def get_default_conan_user_home():
    if 'CONAN_USER_HOME' in os.environ:
        return os.path.join(os.environ['CONAN_USER_HOME'], '.conan')

    return os.path.join(pathlib.Path.home(), '.conan')


def create_profiles(main_args):
    if detected_os() == 'Windows' and not main_args.no_default:
        default_profile = 'vs-2022-MT-Release'
    else:
        default_profile = None

    conan_user_home = get_default_conan_user_home()
    conan_profiles_dir = os.path.join(conan_user_home, 'profiles')

    if not os.path.exists(conan_user_home):
        os.mkdir(conan_user_home)

    if not os.path.exists(conan_profiles_dir):
        os.mkdir(conan_profiles_dir)

    for build_type in all_windows_build_types:
        for compiler in all_windows_compilers:
            for runtime in all_windows_runtimes:
                for address_sanitizer in all_windows_address_sanitizers:
                    profile_name = f'{compiler}-{runtime}-{build_type}'

                    if address_sanitizer:
                        profile_name += "-asan"

                    profile_path = os.path.join(conan_profiles_dir, profile_name)

                    print(f"Creating conan profile {profile_name}")
                    if os.path.exists(profile_path):
                        print(
                            f'Profile {profile_name} already exists, overwriting')

                    args = namedtuple(
                        "create_args", ["compiler", "runtime", "build_type", "address_sanitizer"])
                    args.compiler = compiler
                    args.runtime = runtime
                    args.build_type = build_type
                    args.address_sanitizer = address_sanitizer
                    profile = create_windows_profile(args)
                    with open(profile_path, 'wt') as profile_file:
                        profile_file.write(profile)

                    if profile_name == default_profile:
                        print(f'Using {profile_name} as default profile')
                        with open(os.path.join(conan_profiles_dir, 'default'), 'wt') as profile_file:
                            profile_file.write(profile)


def main():
    argParser = argparse.ArgumentParser(description="Create all linux conan profiles")
    argParser.add_argument("--no-default", default=False, action="store_true", help="Try to guess default profile")

    main_args = argParser.parse_args()
    create_profiles(main_args)


if __name__ == "__main__":
    try:
        sys.exit(main())
    except RuntimeError as err:
        print("Error: {}".format(str(err)))
