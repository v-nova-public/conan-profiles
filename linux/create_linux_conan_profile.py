from string import Template
import argparse
import os
import sys
import subprocess

from conans.client.tools import detected_os, detected_architecture

all_compilers = [
    'gcc-12',
    'gcc-11',
    'gcc-10',
    'gcc-9',
    'gcc-8',
    'gcc-7',
    'gcc-5',
    'clang-11',
    'clang-8',
    'aarch64-linux-gnu-gcc-11',
    'arm-linux-gnueabi-gcc-11',
    'arm-linux-gnueabihf-gcc-11',
    'aarch64-linux-gnu-gcc-9',
    'arm-linux-gnueabi-gcc-9',
    'arm-linux-gnueabihf-gcc-9',
    'aarch64-linux-gnu-gcc-7',
    'arm-linux-gnueabi-gcc-7',
    'arm-linux-gnueabihf-gcc-7',
]

all_build_types = [
    'Release',
    'RelWithDebInfo',
    'Debug'
]

all_address_sanitizers = [
    True,
    False
]


def cmd_output(cmd):
    try:
        return subprocess.check_output(cmd.split(' ')).decode().strip()
    except subprocess.CalledProcessError:
        return None


def detect_glibc_version(compiler):
    gnu_libc_version_source_path = os.path.join(
        os.path.dirname(__file__), 'gnu-libc-version.c')
    cmd = [compiler, gnu_libc_version_source_path, '-o', 'gnu-libc-version']
    p = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    output, error = p.communicate()
    if p.returncode != 0:
        print('Failed to compile gnu-libc-version.c:')
        print(error.decode())
        raise RuntimeError('Failed to determine glibc version')

    cmd = ['./gnu-libc-version']
    p = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    output, error = p.communicate()
    if p.returncode != 0:
        print('Failed to run gnu-libc-version:')
        print(error.decode())
        raise RuntimeError('Failed to determine glibc version')

    os.unlink('gnu-libc-version')

    glibc_version = output.decode().strip()
    return glibc_version


def detect_os_distro():
    with open('/etc/os-release', 'rt') as lsb_release_file:
        lsb_release = lsb_release_file.read()
    lsb_info = lsb_release.split('\n')

    distro_id = next(filter(lambda x: x.startswith('ID='), lsb_info), None)
    distro_release = next(
        filter(lambda x: x.startswith('VERSION_ID='), lsb_info), None)

    if not distro_id:
        raise RuntimeError('Unable to guess Linux distribution')
    if not distro_release:
        raise RuntimeError('Unable to guess Linux distribution release')

    distro_id = distro_id.replace('ID=', '').replace('"', '').lower()
    distro_release = distro_release.replace('VERSION_ID=', '').replace('"', '')

    if distro_id in ['almalinux', 'ol', 'rhel', 'rocky'] or distro_id == 'centos' and distro_release >= '8':
        distro_id = 'el'
        distro_release = distro_release.partition('.')[0]

    return distro_id, distro_release


def get_default():
    distro_id, distro_release = detect_os_distro()
    if distro_id == 'ubuntu':
        if distro_release == '18.04':
            return 'gcc-7-Release'
        elif distro_release == '20.04':
            return 'gcc-9-Release'
        elif distro_release == '22.04':
            return 'gcc-11-Release'
    elif distro_id == 'centos':
        if distro_release == '7':
            return 'gcc-9-Release'
    elif distro_id == 'el':
        if distro_release == '8':
            return 'gcc-12-Release'
    elif distro_id == 'debian':
        if distro_release == '10':
            return 'gcc-8-Release'
        elif distro_release == '11':
            return 'gcc-10-Release'
    return None


def create_profile(args):
    if args.compiler not in all_compilers:
        raise RuntimeError("Unknown compiler: {}".format(args.compiler))

    if args.build_type not in all_build_types:
        raise RuntimeError("Unknown build type: {}".format(args.build_type))

    compiler_name, compiler_version = tuple(args.compiler.split('-')[-2:])
    compiler_target = '-'.join(args.compiler.split('-')[:-2])

    template_path = os.path.join(os.path.dirname(__file__), 'linux.template')
    with open(template_path, 'rb') as template_file:
        profile_template = template_file.read().decode('utf-8')

    conan_os = detected_os()
    conan_arch = detected_architecture()
    conan_arch_build = conan_arch

    cc = cmd_output(f'which {args.compiler}')
    if not cc:
        print(f'Can\'t find C compiler for {args.compiler}, skipping')
        return None

    cxx_compilers = []
    if 'gcc' in args.compiler:
        cxx_compilers = [
            args.compiler.replace('gcc', 'g++'),
        ]
    elif 'clang' in args.compiler:
        cxx_compilers = [
            args.compiler.replace('clang', 'clang++')
        ]

    cxx = next(filter(lambda cxx: cxx, map(lambda cxx_compiler: cmd_output(f'which {cxx_compiler}'),
                                           cxx_compilers)), None)
    if not cxx:
        print(
            f'Can\'t find C++ compiler for {args.compiler}, tried: {cxx_compilers}, skipping')
        return None

    host_compiler = f'{compiler_name}-{compiler_version}'
    glibc_version = detect_glibc_version(host_compiler)
    print(f'Detected glibc version: {glibc_version}')

    distro_id, distro_release = detect_os_distro()
    print(f'Detected Linux distribution: {distro_id} {distro_release}')

    conan_env = {
        'CC': cc,
        'CXX': cxx
    }

    if compiler_name == 'clang':
        conan_env.update({
            'AR': cmd_output(f'which llvm-ar-{compiler_version}'),
            'NM': cmd_output(f'which llvm-nm-{compiler_version}'),
            'LD': cmd_output(f'which llvm-link-{compiler_version}'),
            'STRIP': cmd_output(f'which llvm-strip-{compiler_version}'),
        })
    elif compiler_target == 'aarch64-linux-gnu' or 'arm-linux-gnueabi' in compiler_target:
        conan_env.update({
            'AR': args.compiler.replace('gcc', 'gcc-ar'),
            'NM': args.compiler.replace('gcc', 'gcc-nm'),
        })
        if compiler_target == 'aarch64-linux-gnu':
            conan_arch = 'armv8'
        elif 'arm-linux-gnueabihf' in compiler_target:
            conan_arch = 'armv7hf'
        else:
            conan_arch = 'armv7'

    if 'arm-linux-gnueabi' in compiler_target:
        compiler_libcxx = 'libstdc++'
    else:
        compiler_libcxx = 'libstdc++11'

    extra_compiler_settings = []

    if args.address_sanitizer:
        extra_compiler_settings.append('compiler.address_sanitizer=True')

        conan_env.update({
            'CFLAGS': '-fsanitize=address',
            'CXXFLAGS': '-fsanitize=address',
            'LDFLAGS': '-fsanitize=address'
        })

    profile = Template(profile_template).substitute({
        'os': conan_os,
        'arch': conan_arch,
        'arch_build': conan_arch_build,
        'build_type': args.build_type,
        'compiler_name': compiler_name,
        'compiler_version': compiler_version,
        'glibc_version': glibc_version,
        'compiler_libcxx': compiler_libcxx,
        'extra_compiler_settings': '\n'.join(extra_compiler_settings),
        'os_distro': distro_id,
        'os_distro_version': distro_release,
        'env': '\n'.join(map(lambda e: f'{e[0]}={e[1]}', conan_env.items()))
    })

    return profile


def main():
    argParser = argparse.ArgumentParser(
        description="Create conan profile for building libraries for specific android arch/api/abi")
    argParser.add_argument("--build-type", default="Release", help="Specify build type")
    argParser.add_argument("--compiler", required=True, help="Specify compiler to use")

    args = argParser.parse_args()

    profile = create_profile(args)
    print(profile)


if __name__ == "__main__":
    try:
        sys.exit(main())
    except RuntimeError as err:
        print("Error: {}".format(str(err)))
