#!/bin/python

import os
import sys

# ---------------------------------------------------------------------------------------
# This is a hack to allow create_all_linux_conan_profiles.py to be callable directly
# so that the legacy systems not using the top-level entry-point keep working.
# We should delete this as soon as we've transitioned everything onto the new entry-point
if __name__ == "__main__":
    dir = os.path.dirname(os.path.dirname(__file__))
    sys.path.append(dir)
# ---------------------------------------------------------------------------------------

from linux.create_linux_conan_profile import create_profile as create_linux_profile
from linux.create_linux_conan_profile import all_compilers as all_linux_compilers
from linux.create_linux_conan_profile import all_build_types as all_linux_build_types
from linux.create_linux_conan_profile import get_default as get_default_linux_profile
from linux.create_linux_conan_profile import all_address_sanitizers as all_linux_address_sanitizers

from conans.client.tools import detected_architecture

from collections import namedtuple

import pathlib
import argparse


def get_default_conan_user_home():
    if 'CONAN_USER_HOME' in os.environ:
        return os.path.join(os.environ['CONAN_USER_HOME'], '.conan')

    return os.path.join(pathlib.Path.home(), '.conan')


def create_profiles(main_args):
    conan_user_home = get_default_conan_user_home()
    conan_profiles_dir = os.path.join(conan_user_home, 'profiles')

    if not os.path.exists(conan_user_home):
        os.mkdir(conan_user_home)

    if not os.path.exists(conan_profiles_dir):
        os.mkdir(conan_profiles_dir)

    arch = detected_architecture()

    compilers = all_linux_compilers
    if 'arm' in arch:
        compilers = list(filter(
            lambda compiler: not ('clang' in compiler or compiler.startswith('arm-linux') or compiler.startswith('aarch64-linux')),
            compilers))

    default_profile = get_default_linux_profile() if not main_args.no_default else None

    for build_type in all_linux_build_types:
        for compiler in compilers:
            for address_sanitizer in all_linux_address_sanitizers:
                profile_name = f'{compiler}-{build_type}'

                if address_sanitizer:
                    profile_name += "-asan"

                if arch == 'armv8' and not compiler.startswith('aarch64'):
                    profile_name = f'aarch64-linux-gnu-{profile_name}'

                profile_path = os.path.join(conan_profiles_dir, profile_name)

                print(f"Creating conan profile {profile_name}")
                if os.path.exists(profile_path):
                    print(f'Profile {profile_name} already exists, overwriting')

                args = namedtuple("create_args", ["compiler", "build_type", "address_sanitizer"])
                args.compiler = compiler
                args.build_type = build_type
                args.address_sanitizer = address_sanitizer
                profile = create_linux_profile(args)
                if not profile:
                    print(f'Unable to create profile {profile_name}, skipping')
                    continue

                with open(profile_path, 'wt') as profile_file:
                    profile_file.write(profile)

                if default_profile and profile_name == default_profile:
                    print(f'Using {profile_name} as default profile')
                    with open(os.path.join(conan_profiles_dir, 'default'), 'wt') as profile_file:
                        profile_file.write(profile)


def main():
    argParser = argparse.ArgumentParser(description="Create all linux conan profiles")
    argParser.add_argument("--no-default", default=False, action="store_true", help="Try to guess default profile")

    main_args = argParser.parse_args()
    create_profiles(main_args)


if __name__ == "__main__":
    try:
        sys.exit(main())
    except RuntimeError as err:
        print("Error: {}".format(str(err)))
